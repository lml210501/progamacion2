from collections import namedtuple

# Basic example
Point = namedtuple('Point', ['x', 'y'])
p = Point(11, y=22)     # instantiate with positional or keyword arguments
p[0] + p[1]             # indexable like the plain tuple (11, 22)
p.x + p.y               # but fields also accessible by name

x, y = p                # unpack like a regular tuple

p                       # readable __repr__ with a name=value style