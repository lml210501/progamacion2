import collections
import itertools
from collections import defaultdict

e = defaultdict(lambda: 'not_grade')
e.update(john='A', mary='A+')
print(e['john'])
print(e['peter'])


def constant_factory(value):
    return lambda: value


grades = defaultdict(constant_factory('<missing>'))
grades.update(john='A', mary='A+')

print(grades['carlos'])


s = [('yellow', 1), ('blue', 2), ('yellow', 3), ('blue', 4), ('red', 1)]
d = defaultdict(list)
for k, v in s:
    d[k].append(v)

sorted(d.items())

s = 'mississippi'
d = defaultdict(int)
for k in s:
    d[k] += 1

sorted(d.items())


s = [('john', 'calculus', 'A'), ('mary', 'calculus', 'A'), ('mary', 'english', 'B'),
     ('mary', 'algebra', 'A'), ('john', 'algebra', 'B+'), ('john', 'english', 'C')]
d = defaultdict(dict)
for name, course, grade in s:
    d[name].update({course: grade})


cities_by_country = {
    "Madrid": "ES",
    "Tokio": "JPN",
    "Sevilla": "ES",
    "London": "UK",
    "Bristol": "UK",
    "York": "UK",
    "Vancouver": "CA",
    "Toronto": "CA",
    "San Mateo": "US",
}

# No deaultdict
dct = {}
for city, country in cities_by_country.items():
    if country in dct:
        dct[country].append(city)
    else:
        dct[country] = [city]

print("Deftaul_dict")
print(dct)

# No deaultdict
dct = collections.defaultdict(list)
for city, country in cities_by_country.items():
    dct[country].append(city)
print("Deftaul_dict true")
print(dct)


print("groupby")
an_iterator = itertools.groupby(cities_by_country.items(), lambda x: x[1])

for key, group in an_iterator:
    key_and_group = {key: list(group)}
    print(key_and_group)
