from typing import Callable


def make_function(n: int) -> Callable:
    def inner(x: int) -> int:
        return x ** n
    return inner


f = make_function(3)
print(f(4))

import re

if re.fullmatch(r'\d+', '9990a'):
    print('OK')
else:
    print('No match')