from typing import Callable


def apply(x: float, f: Callable) -> float:
    if not isinstance(x, float):
        raise ValueError
    result = f(x)
    return result


def add(y: float) -> float:
    return y + 3


def mult(y: float) -> float:
    return y * 10.0


print(apply(5.0, mult))
print(apply(10.0, add))

