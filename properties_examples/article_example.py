import logging.config

logging.config.fileConfig('../logger_example/logger.conf')


class CapitalizeName:
    def __get__(self, instance, owner):
        return instance._name

    def __set__(self, instance, value:str):
        print(f"Setter method with value {value}")
        if not isinstance(value, str):
            raise Exception("No me vale")
        complete_name = value.split(" ")
        instance._name = complete_name[0].capitalize()
        try:
            instance.apellido = complete_name[1]
        except IndexError:
            pass

class Person:
    name = CapitalizeName()

    def __init__(self, name, email, age):
        self.name = name
        self.email = email
        self.age = age

person = Person("Jorge Manzanares", "jorge.manza1@gmai.com", 28)

print(vars(person))
logger = logging.getLogger("simpleLogger")

class Article:
    categoria = CapitalizeName()

    def __init__(self, article_id:str):
        self.article_id = article_id
        self._price = None

    @property
    def price(self):
        """Article price"""
        return self._price

    @price.setter
    def price(self, new_price):
        if isinstance(new_price, float) and new_price > 0:
            self._price = new_price
        else:
            print("Please enter a valid price")

    @price.deleter
    def price(self):
        del self._price

    def __str__(self):
        if hasattr(self, '_price'):
            return f"Article['{self.article_id}', {self._price}]"
        else:
            return f"Article['{self.article_id}']"


if __name__ == '__main__':
    art = Article('0001')
    art.price = 'a'
    art.price = 2.3
    print(art)
    del art.price
    print(art)
