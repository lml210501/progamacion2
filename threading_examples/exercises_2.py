import os
import threading
import time

chopping = True

def vegetable_chopper():
    name = threading.currentThread().getName()
    vegetable_count = 0
    while chopping:
        print(f"{name} is chopping vegetables")
        vegetable_count += 1
    print(f"{name} chopped {vegetable_count} vegetables")

print(f"Process id: {os.getpid()}")
print(f"threads count: {threading.active_count()}")

if __name__ == "__main__":
    threading.Thread(name="Ana", target=vegetable_chopper).start()
    threading.Thread(name="George", target=vegetable_chopper).start()
    time.sleep(2)
    chopping = False

