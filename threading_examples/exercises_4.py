from threading import Thread

garlic_count = 0


def shopper():
    global garlic_count
    for i in range(100000):
        garlic_count += 1

if __name__ == "__main__":
    george = Thread(target=shopper, name="George")
    ana = Thread(target=shopper, name="Ana")
    ana.start()
    george.start()
    ana.join()
    george.join()
    print(f"We should buy {garlic_count}")
