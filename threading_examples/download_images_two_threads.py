import requests
import time
import os
import threading


img_urls = [
    'http://ipv4.download.thinkbroadband.com/20MB.zip',
    'http://speedtest4.tele2.net/100MB.zip',
]

def download_image(img_url: str, tid: int):
    print(f'Downloading {img_url}...')
    img_bytes = requests.get(img_url).content
    img_name = img_url.split('/')[3] + str(tid)
    with open(os.path.join('images', img_name), 'wb') as img_file:
        img_file.write(img_bytes)
        print(f'{img_name} was downloaded...')


if __name__ == '__main__':
    t1 = time.perf_counter()

    t20 = threading.Thread(target=download_image, name='t-20MB', args=(img_urls[0], 0))
    t50 = threading.Thread(target=download_image, name='t-50MB', args=(img_urls[1], 1))

    t20.start()
    t50.start()

    t20.join()
    t50.join()

    # Join both threads so that perf_counter is not execute until both threads finish
    t2 = time.perf_counter()

    print(f'Finished in {t2-t1} seconds')


"""
Using ethernet cable UFV, 8.2 seconds
"""