import os
import time



def do_something(seconds):
    print(f'Sleeping {seconds} second(s)...')
    print('parent process:', os.getppid())
    print('process id:', os.getpid())
    time.sleep(seconds)
    print(f'Done Sleeping...{seconds}')


if __name__ == '__main__':
    start = time.perf_counter()
    do_something(1)
    do_something(1)

    finish = time.perf_counter()

    print(f'Finished in {round(finish-start, 2)} second(s)')

# Modified from https://www.youtube.com/watch?v=IEEhzQoKtQU
# https://github.com/rvlucerga/code_snippets/blob/master/Python/Threading/threading-demo.py