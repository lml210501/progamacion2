import os
import threading
import time

chopping = True

def kitchen_cleaner():
    while True:
        print(f"{threading.currentThread().name} is cleaning the kitchen")
        time.sleep(5)


if __name__ == "__main__":
    thread = threading.Thread(name="Robot", target=kitchen_cleaner)
    thread.daemon = True
    thread.start()

    # print("Main thread is cooking...")
    # time.sleep(0.6)
    print("Main thread is cooking...")
    time.sleep(0.6)
    print("Main thread is cooking...")
    time.sleep(0.6)
    print("Main thread is cooking...")
    time.sleep(0.6)
    print("MainThread is done!")

