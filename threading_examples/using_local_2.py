from threading import Thread, local
from time import sleep


def worker(local_data: local):
    local_data.message = 'end'
    for i in range(0, 5):
        print('.', end='', flush=True)
        sleep(0.2)
    print(local_data.message)


print('Starting')
data = local()
t = Thread(target=worker, args=(data,))
t.start()
t.join()
try:
    print(local.message)
except AttributeError:
    print('message not set')
print('\nDone')
