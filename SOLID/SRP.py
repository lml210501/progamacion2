from typing import List

import numpy as np


def math_operations(list_: List[int]) -> None:
    # Compute Average
    print(f"the mean is {np.mean(list_)}")
    # Compute Max
    print(f"the max is {np.max(list_)}")


math_operations([1, 2, 3, 4, 5])
# the mean is 3.0
# the max is 5


def get_mean(list_: List[int]) -> None:
    """Compute Mean"""
    print(f"the mean is {np.mean(list_)}")


def get_max(list_: List[int]) -> None:
    """Compute Max"""
    print(f"the max is {np.max(list_)}")


def main(list_: List[int]):
    # Compute Average
    get_mean(list_)
    # Compute Max
    get_max(list_)


main([1, 2, 3, 4, 5])
# the mean is 3.0
# the max is 5
