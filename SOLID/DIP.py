
class Lampara:
    def on(self):
        print("Encendiendo Lampara")

    def off(self):
        print("Apagando Lampara")


class Boton:
    def __init__(self, lampara:Lampara):
        self.lampara = lampara

    def pulsar_encender_boton(self):
        self.lampara.on()

    def pulsar_apagar_boton(self):
        self.lampara.off()

