from abc import abstractmethod


class OrderServiceInterface:
    @abstractmethod
    def oder_burger(self, quantity):
        pass

    @abstractmethod
    def order_fries(self, quantity):
        pass

    @abstractmethod
    def order_combo(self, fries, burgers):
        pass


class BurgerStore(OrderServiceInterface):
    def oder_burger(self, quantity):
        print(f"We receive the order! Quantity of burgers {quantity}")

    def order_fries(self, quantity):
        raise Exception("Burger store only accepts burgers.")

    def order_combo(self, fries, burgers):
        raise Exception("Burger store only accepts burgers.")


class Animal:
    @staticmethod
    def swim():
        print("Can swim")

    @staticmethod
    def walk():
        print("Can walk")


class Human(Animal):
    @staticmethod
    def swim():
        return print("Humans can swim")

    @staticmethod
    def walk():
        return print("Humans can walk")


class Whale(Animal):
    @staticmethod
    def swim():
        return print("Whales can swim")


Human.swim()
Human.walk()

Whale.swim()
Whale.walk()


class Walker:
    @staticmethod
    def walk():
        print("Can Walk")


class Swimmer:
    @staticmethod
    def swim():
        print("Can Swim")


class Human(Walker, Swimmer):
    @staticmethod
    def swim():
        print("Humans can swim")

    @staticmethod
    def walk():
        print("Humans can walk")


class Whale(Swimmer):
    @staticmethod
    def swim():
        print("Whales can swim")

Human.walk()
Human.swim()

Whale.swim()
Whale.walk()

