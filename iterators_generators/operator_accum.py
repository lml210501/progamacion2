from itertools import accumulate
import operator_accum

data = [3, 4, 6, 2, 1, 9, 0, 7, 5, 8]
a = list(accumulate(data, operator_accum.mul))     # running product
print(a)

b = list(accumulate(data, max))              # running maximum
print(b)