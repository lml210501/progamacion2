import pandas as pd

win = {
    "win": 6,
    "lose": 0,
    "draw": 3
}
convert = {
    "A": "X",
    "B": "Y",
    "C": "Z"
}
selected = {
    "X": 1,
    "Y": 2,
    "Z": 3
}

# dataframe = pd.read_csv("input.txt", sep=" ", names=["col1", "col2"])
serie_1 = pd.Series(["A", "B", "C"])
serie_2 = pd.Series(["Y", "X", "Z"])
dataframe = pd.DataFrame({"col1":serie_1, "col2":serie_2})
print(dataframe.head())


def winner(dataframe_):
    player1 = dataframe_["col1"]
    player2 = dataframe_["col2"]
    total_points = 0
    if (player1 == "A" and player2 == "Y") or (player1 == "B" and player2 == "Z") or (
            player1 == "C" and player2 == "X"):
        total_points += 6
    elif convert[player1] == player2:
        total_points += 3
    total_points += selected[player2]
    return total_points


dataframe["total_points"] = dataframe.apply(winner, axis=1)

print(dataframe.head())
print(sum(dataframe["total_points"]))
