import pandas as pd

beats = {'A': 'C', 'C': 'B', 'B': 'A'}
loses = {}
for key, value in beats.items():
    loses[value] = key
transform = {
    "X": "A",
    "Y": "B",
    "Z": "C"
}
selected = {
    "A": 1,
    "B": 2,
    "C": 3
}

dataframe = pd.read_csv("input.txt", sep=" ", names=["col1", "col2"])
# serie_1 = pd.Series(["A", "B", "C"])
# serie_2 = pd.Series(["Y", "X", "Z"])
# dataframe = pd.DataFrame({"col1":serie_1, "col2":serie_2})
print(dataframe.head())


def winner(dataframe_):
    player1 = dataframe_["col1"]
    player2 = dataframe_["col2"]
    total_points = 0
    if beats[player2] == player1:
        total_points += 6
    elif player1 == player2:
        total_points += 3
    total_points += selected[player2]
    return total_points


def fill_col2(dataframe_):
    player1 = dataframe_["col1"]
    result = dataframe_["resultado"]
    if result == "Y":
        return player1
    if result == "X":
        return beats[player1]
    else:
        return loses[player1]


dataframe["resultado"] = dataframe["col2"]
dataframe["col2"] = dataframe.apply(fill_col2, axis=1)
dataframe["total_points"] = dataframe.apply(winner, axis=1)

print(dataframe.head())
print(sum(dataframe["total_points"]))
