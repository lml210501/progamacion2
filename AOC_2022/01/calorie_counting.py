

from collections import defaultdict
import heapq as hq

with open("input.txt") as file:
    items = []
    lines = file.read().split("\n\n")

    for line in lines:
        hq.heappush(items, sum([int(num) for num in line.split("\n")]))


print(max(items))
# extra
print(sum(hq.nlargest(3, items)))
