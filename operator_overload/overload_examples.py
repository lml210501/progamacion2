from math import pow


class Complex:
    def __init__(self, real: float, imag: float):
        self.real = real
        self.imag = imag

    def __eq__(self, other):
        return self.real == other.real and self.imag and other.imag

    def mod(self):
        return pow(self.real ** 2 + self.imag ** 2, 0.5)

    def __add__(self, other):
        return complex(self.real + other.real, self.imag + other.imag)

    def __sub__(self, other):
        return complex(self.real - other.real, self.imag - other.imag)

    def __mul__(self, other):
        return complex(self.real * other.real - self.imag * other.imag,
                       self.real * other.imag + self.imag * other.real)

    def __str__(self, precision=2):
        return str(("%." + "%df" % precision) % float(self.real)) + (
            '+' if self.imag >= 0 else '-') + str(
            ("%." + "%df" % precision) % float(abs(self.imag))) + 'i'


a = Complex(1, 2)
b = Complex(3, 4)
print("Addition: " + str(a + b))


print(a == b)


print("Modulas of complex Number A: " + str(a.mod()))

a = 60            # 60 = 0011 1100
b = 13            # 13 = 0000 1101
c = 0

c = a & b         # 12 = 0000 1100
