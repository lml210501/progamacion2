from setuptools import setup, find_packages

setup(
    name='NOMBRE_PROYECTO',
    version='VERSION = M.m.f',
    license='', # si se usa licencia
    author='Autor',
    author_email='email',
    description='descripcion',
    test_suite='tests',
    tests_require=[],
    packages=find_packages(include=['process_repo']),
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    install_requires=[
        'Librerias que necesitamos'
    ],
)
