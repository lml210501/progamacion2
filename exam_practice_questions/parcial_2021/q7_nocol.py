class Cart(dict):

    def __add__(self, other: 'Cart') -> 'Cart':
        return Cart(**self, **other)


cart_1 = Cart(pen=1, pencil=2)
cart_2 = Cart(rubber=2, paper=8)

total = cart_1 + cart_2

print(total['pencil'])  # returns 2
type(total)  # returns <class '__main__.Cart'>


class Cart(dict):

    def __sub__(self, other: 'str') -> 'Cart':
        cart = Cart(**self)
        del cart[other]
        return cart


cart_1 = Cart(pen=1, pencil=2)
item = 'pen'

total = cart_1 - item

print(total)  # returns {'pencil': 2}
type(total)  # returns <class '__main__.Cart'>
