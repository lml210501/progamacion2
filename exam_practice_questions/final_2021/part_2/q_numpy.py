import numpy as np

mu, sigma = 50, 20
r = np.round(np.random.normal(mu, sigma, 10),0).astype(np.int64)
a = np.repeat(50, 10)
t = np.maximum(r - a, 0)
p = t * 60
m = np.mean(p)
