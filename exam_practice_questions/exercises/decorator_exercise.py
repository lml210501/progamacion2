# Crear un decorador que controle errores.
# Se podrá especificar un parámetro que indique el tipo o tipos de datos que esperamos.
# si espero una lista de numeros, debera devolver un error TypeError indicando el error.
# Crear dos funciones
#       La primera va a sumar un array de numeros y deberá dar un error en caso contrario
#       La segunda va a devolver la suma del primer y ultimo elemento de una secuencia. Para eso,
#          deberá admitir solo secuencias y dar un error en caso contrario.
# Puede ser el mismo error o un error similar.


# Crear un decorador que convierta la salida de las funciones a una respuesta similar a la que devuelve una petición
# http: se considera una salida http desde python como una tupla de status_code y mensaje. En caso de encontrar alguna
# excepción es necesario informar en el mensaje con una tupla de 3 elementos.
