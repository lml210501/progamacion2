import heapq as hq
import logging
from dataclasses import dataclass
from typing import Generator
import random
import time

"""heapq"""
# Crea un programa que encuentre el elemento N ( 1 <= N <= array_length)
# más largo de una lista desordenada utilizando heap.


def find_Kth_Largest(nums, k):
    """
    :type nums: List[int]
    :type of k: int
    :return value type: int
    """
    h = []
    for e in nums:
        hq.heappush(h, (-e, e))
    for i in range(k):
        w, e = hq.heappop(h)
        if i == k - 1:
            return e

arr_nums = [12, 14, 9, 50, 61, 41]
result = find_Kth_Largest(arr_nums, 3)
print("Third largest element:",result)
result = find_Kth_Largest(arr_nums, 2)
print("\nSecond largest element:",result)
result = find_Kth_Largest(arr_nums, 5)
print("\nFifth largest element:",result)

#  Write a Python program to find the three largest integers from a given list of numbers using Heap queue algorithm.
# Utilizando heap, crea una funcion que encuentre los 3 numeros más grandes de una lista.
nums_list = [25, 35, 22, 85, 14, 65, 75, 22, 58]
print("Original list:")
print(nums_list)
# Find three largest values
largest_nums = hq.nlargest(3, nums_list)
print("\nThree largest numbers are:", largest_nums)


# Escribir un programa que ejecute el producto de los 3 numeros más grandes de una lista.
def maximumProduct(nums):
    a = hq.nlargest(3, nums)
    return a[0] * a[1] * a[2]
arr_nums = [12, 74, 9, 50, 61, 41]
print("Original array elements:")
print(arr_nums)
print("Maximum product of three numbers of the said array:")
print(maximumProduct(arr_nums))


"""High order functions"""

# Utilizando higher order function de forma manual, dadao una lista, devuelva la misma con la función aplicada.
# Crear esto para que devuelva todos los elemenos en mayuscula, para que devuelva la version en string the una lista de numeros,
# y para que capitalice una lista de strings. Son 3 llamadas a la misma funcion.


def applyToEachObject(function, llist):
    myList = []
    for item in llist:
        myList.append(function(item))
    return myList


allUpperCase = applyToEachObject(str.upper, ['Building', 'ROAD', 'tree'])
allNumbers = applyToEachObject(str, [1,2,3,4,5])
allCapitalize = applyToEachObject(str.capitalize, ['Building', 'ROAD', 'tree'])
print(allCapitalize)
print(allNumbers)
print(allUpperCase)

#  Utilizando map, escribe un programa que multiplique los números de una lista por 3.

print( list(map(lambda x: x * 3, [1,2,3,4,5,6,7])))

#  Crear un programa que sume tres listas utilizando map y lambda.

print(list(map(lambda x, y, z: x + y + z, [1,2,3], [4,5,6], [7,8,9])))

#  Crear un programa que convierta los objetos de una lista a mayuscula y minuscula y eliminar los duplicados.
# Utilizar map.

chrars = {'a', 'b', 'E', 'f', 'a', 'i', 'o', 'U', 'a', 1}
print("Letras y caracteres", chrars)

result = map(lambda s: (str(s).upper(), str(s).lower()), chrars)
print("Desdepues de crear el mapa con las mayusculas y minusculas eliminamos duplicados convirtiendolo en set:")
print(set(result))

"""Logging info"""

#  Create a logger configuration programmatically that have two different handlers, one will be a handler for writing
#    error or exeption messages to the file and the other handler will be to print in the console messages, allowing
#    all type of levels for this one.
#    For the file handler we will use a complex formatter like:
#          - [ %(2022-10-04) - %(LOGGERS_NAME) - %(INFO) - %(current_file)s:%(49)d ] %(This is the log message)s
#   The stream handler will use a different logging formatter like:
#          - %(INFO)s - %(LOGGERS_NAME)s %(asctime)s - %(message)s
#   Note: you will need to replace the variables names inside %() for this to work as expected.
logger = logging.getLogger("mi_logger")
file_handler = logging.FileHandler('mi_archivo.txt')
formatter_extend = logging.Formatter("[ %(asctime)s - %(name)s - %(levelname)s - %(filename)s:%(lineno)d ] %(message)s")
file_handler.setFormatter(formatter_extend)
file_handler.setLevel("ERROR")

file_handler_2 = logging.StreamHandler()
formatter_simple = logging.Formatter("%(levelname)s - %(name)s %(asctime)s - %(message)s")
file_handler_2.setFormatter(formatter_simple)
file_handler_2.setLevel("DEBUG")

logger.addHandler(file_handler)
logger.addHandler(file_handler_2)
logger.setLevel(logging.DEBUG)
logger.info("Prueba")
logger.error("Error")

"""Classes"""
#  Create an Alumno class and prevent the modification of the attributes inside it.
#  Do not use dataclass for this exercise.

class InmutableException(Exception):
    """Exception to be thrown if someone tries to modify an attribute from Alumno"""
    pass
class Alumno:

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __setattr__(self, key, value):
        if not hasattr(self, key):
            super().__setattr__(key, value)
        else:
            raise InmutableException("Can't modify immutable object's attribute: {}".format(key))

a = Alumno("test", 15)
try:
    a.name = "name"
except InmutableException:
    print("No me deja modificar")
print(a.name)

# Crear una lista de clases PEDIDO que a parte de otras variables, puede tener una variable tipo float que marca
# el importe del pedido, puede venir definida de cualquier manera. Quiero poder crear una lista en la que los indices
# de la lista creada marquen el numero de decimales maximos que tiene que tener dicho pedido. Si esta el pedido1 en el
# indice 1 se tendra que redondear el importe a 1 solo decimal. Si estuviera en el indice 5, tendriamos que redondearlo
# a 5 decimales.

@dataclass
class Order:
    """Class to store data from an Order"""
    amount: float = 0.


lista = [Order(), Order(random.random()*10), Order(random.random()*10)]

print(f"Lista: {lista}")
for num_decimals in range(len(lista)):
    print(round(lista[num_decimals].amount, num_decimals+1))

# Crear una clase Vehiculo, Motocicleta y Coche.
# Instancia una clase u otra en funcion de las ruedas que tengas de inicio, si tienes 4 patas será un coche,
# si tienes dos será una Motocicleta. PAra este ejercicio es importante el uso de herencia y polimorfismo.
# Cualquier instancia de vehiculo puede arrancar, todos tienen un numero de ruedas, una potencia, una velocidad maxima
# y una aceleración.


class Vehiculo:
    @staticmethod
    def init(ruedas, potencia, velocidad, acceleracion):
        Inst = Motocicleta if ruedas == 2 else Coche
        return Inst(ruedas, potencia, velocidad, acceleracion)

    def arrancar(self):
        print("arrancando vehiculo!")


class Motocicleta(Vehiculo):
    def __init__(self, ruedas, potencia, velocidad, aceleracion):
        pass

    def arrancar(self):
        print("Arrancando motocicleta!")


class Coche(Vehiculo):
    def __init__(self, ruedas, potencia, velocidad, aceleracion):
        pass

    def arrancar(self):
        print("Arrancando coche!")


Vehiculo.init(2, 100, 150, 2).arrancar()
Vehiculo.init(4, 100, 150, 2).arrancar()


# Crear un generador que recorra de forma indefinida una cadena de texto, caracter a caracter, hasta que se introduzca
# en el generador un string "Exit", en cuyo caso terminará.


def generator_string_sequence(text: str) -> Generator[str, str, None]:
    """Generator to loop over the given string by characters until it gets 'Exit'."""
    infinite = True
    while infinite:
        for c in text:
            _exit = yield c
            if _exit == 'Exit':
                infinite = False
                break


i = 0
generator = generator_string_sequence("Hola")
try:
    while True:
        print(next(generator))
        i += 1
        if i == 3:
            generator.send('Exit')
except StopIteration as e:
    print("Finish iterating")

# Crear un generador que me devuelva números aleatorios y siempre que se vuelva a ejecutar, me devuelva la diferencia
# de tiempo desde la ultima ejecución del generador. (1, 0) , (8, 1.24352). Si la diferencia de tiempo es mayor que 5 segundos
# deberá lanzar una excepcion y terminar el problema.


def generator_func_with_time_interval() -> Generator[int, None, None]:
    """
    Generate a Tuple(int, float) until the execution time from the previous call exceeds 5 seconds.
    It will raise a StopIteration then.
    The first element in the tuple is the random integer generated.
    The second element is the time frame since the previous execution.
    """
    end_time = time.perf_counter()
    while time.perf_counter()-end_time < 5:
        time_took = round(time.perf_counter()-end_time, 2)
        end_time = time.perf_counter()
        yield random.randint(0, 100), time_took


randoms = generator_func_with_time_interval()
try:
    while True:
        print(next(randoms))
        sleep = random.random() * 6
        print(f"Sleeping {sleep}")
        time.sleep(sleep)
except StopIteration as e:
    print("It took more than 5 seconds to execute.")
