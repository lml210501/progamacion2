
class ErrorCheck:

    def __init__(self, types):
        """
        If there are decorator arguments, the function
        to be decorated is not passed to the constructor!
        """
        print("Inside __init__()")
        self.types = types

    def __call__(self, f):
        """
        If there are decorator arguments, __call__() is only called
        once, as part of the decoration process! You can only give
        it a single argument, which is the function object.
        """
        print("Inside __call__()")

        def wrapped_f(*args):
            print("Inside wrapped_f()")
            print("Decorator arguments:", self.types)
            if any([not isinstance(i, self.types) for i in args]):
                raise TypeError("parameter cannot be a string !!")
            else:
                value = f(*args)
            print("After f(*args)")
            return value

        return wrapped_f


@ErrorCheck(int)
def add_numbers(*numbers):
    return sum(numbers)


@ErrorCheck((str, tuple, list))
def first_last(sequence):
    return sequence[:1] + sequence[-1:]


#  returns 6
print(add_numbers(1, 2, 3.0))

print(first_last("one"))
print(first_last([1,2,3,4]))
print(first_last((1,2,3,4,5,6)))
print(first_last({1,2,3,4,5,6}))
