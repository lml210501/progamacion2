import itertools
from functools import reduce

import requests

url = requests.get("https://api.open-meteo.com/v1/forecast?latitude=40.4167&longitude=-3.7033&hourly=temperature_2m,apparent_temperature&current_weather=true&past_days=4")
json_response = url.json()
print(json_response)
