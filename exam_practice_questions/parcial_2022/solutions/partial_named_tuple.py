import functools
import math
from collections import namedtuple

Point = namedtuple("Point", "x y")


def dist(point_1: Point, point_2: Point) -> float:
    return math.sqrt(((point_1.x - point_2.x) ** 2) + (point_1.y - point_2.y) ** 2)


partial_dist = functools.partial(dist, Point(2, 2))
print(f"Distance: {partial_dist(Point(1,1))}")
