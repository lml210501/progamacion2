import heapq
from dataclasses import dataclass, field
from typing import List


class Customer:

    def __init__(self, cesta):
        self.cesta = cesta

    @property
    def time(self):
        return self.time


lista = [Customer(["Colacao", "Pera"]), Customer(["Cocacola", "Platano"])]
cajas = 2
expected = 2

def shopping_center(customers: List[Customer], n: int) -> int:
    cajas = [0] * n
    for customer in customers:
        heapq.heapreplace(cajas, cajas[0] + customer.time)
    return max(cajas)


print("Tiempo:", shopping_center(lista, cajas))


###################################################################

class Customer:
    def __init__(self, articles=None):
        if articles is None:
            articles = list()
        self._articles = articles
        self._time = len(articles)

    @property
    def time(self) -> int:
        return self._time

    @property
    def articles(self):
        return self._articles

    def __add__(self, other):
        return Customer(self.articles + other.articles)

    def __lt__(self, other):
        return self.time < other.time

    def __str__(self):
        return f"{self.time}"


def queue_time(customers: List[Customer], n):
    heap = [Customer()] * n
    # heap = [0] * n
    for c in customers:
        heapq.heapreplace(heap, heap[0] + c)
    return max(heap)


print(f"El tiempo que tarda en despejarse la cola es: {queue_time([Customer(['Banana', 'Platano', 'Judias']), Customer(['Cola'])], 2)}")

print(f"El tiempo que tarda en despejarse la cola es: {queue_time([Customer([10]), Customer([5]), Customer([5]), Customer([5]), Customer([6])], 5)}")


def queue_time_easy(customers, n):
    heap = [0] * n
    for customer in customers:
        heapq.heapreplace(heap, heap[0] + customer.time)
    return max(heap)



