import itertools
adjacents = adj = {
    "1": "124",
    "2": "2135",
    "3": "326",
    "4": "4157",
    "5": "52468",
    "6": "6359",
    "7": "748",
    "8": "85790",
    "9": "968",
    "0": "08",
}

def get_pins(observed):
    if len(observed) == 1:
        return adjacents[observed]
    return [a + b for a in adjacents[observed[0]] for b in get_pins(observed[1:])]

expected = ['236', '238', '239', '256', '258', '259', '266', '268', '269', '296', '298', '299', '336', '338', '339',
            '356', '358', '359', '366', '368', '369', '396', '398', '399', '636', '638', '639', '656', '658', '659',
            '666', '668', '669', '696', '698', '699']
print(len(expected))
print(sorted(expected))
print(sorted(get_pins("369")))
