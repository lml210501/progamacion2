import math
from collections import namedtuple
from dataclasses import dataclass
from functools import partial
from typing import NamedTuple

# Point = namedtuple("Point2", "x, y")

@dataclass
class Figura:
    __slots__ = ("x", "y")


class Point3D(Figura):
    __slots__ = "z"


#
# def euclidean_distance(point_1: Point, point_2: Point) -> float:
#     return math.sqrt(point_1.x- point_2.x + point_1.y- point_2.y)
#
#
# distance_2_2 = partial(euclidean_distance, Point(2, 2))
#
# print(distance_2_2(Point(0,0)))
#

p = Figura()

# p.clase = 3

print(p)
