
class ReduceInteger:

    def __get__(self, instance, owner):
        value = instance._allowance
        instance._allowance -= 1
        instance._allowance = max(0, instance._allowance)
        return value

    def __set__(self, instance, value):
        instance._allowance = value

    def __delete__(self, instance):
        delete = input("Seguro que quieres borrar la variable allowance?")
        if delete.lower() == 'y':
            print(f"Se ha eliminado: {instance._allowance}")
            del instance._allowance
        else:
            print("La variable no se ha borrado")


class Capacidad:
    allowance = ReduceInteger()

    def __init__(self, allowance):
        self.allowance = allowance


# c = Capacidad(1)
# c.allowance = 1
# print(c.allowance)
# print(c.allowance)
# print(c.allowance)
# del c.allowance
# print(c.allowance)

####################################


class ReduceInteger2:
    def __set_name__(self, owner, name):
        pass

    def __get__(self, instance, owner):
        value = instance._allowance
        instance._allowance = max(0, instance._allowance)
        return value

    def __set__(self, instance, value):
        if instance._allowance == 0:
            print("No te dejo modificar")
        else:
            instance._allowance = value


class Capacidad2:
    allowance = ReduceInteger2()

    def __init__(self, allowance = 0):
        self._allowance = allowance
        self.allowance = allowance


c = Capacidad2(2)
print(c.allowance)
print(c.allowance)
print(c.allowance)
print(c.allowance)
c.allowance = 3
print(c.allowance)