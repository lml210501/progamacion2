import csv
from collections import defaultdict
from dataclasses import dataclass, field
from typing import Callable


def deprecated(active=True):
    def _deprecated(func):
        def wrapper(*args, **kwargs):
            if active:
                print("This method is deprecated, please consider another_function")
            return func(*args, **kwargs)

        return wrapper
    return _deprecated


class ReversedCityIterator:
    def __init__(self, cities):
        cities.sort(key=lambda item: len(item), reverse=True)
        self._cities: list = cities

    def __next__(self):
        if len(self._cities) == 0:
            print("No more cities REVERSED")
            raise StopIteration()
        return self._cities.pop()


class CityIterator:
    def __init__(self, cities):
        cities.sort(key=lambda item: len(item))
        self._cities: list = cities

    def __next__(self):
        if len(self._cities) == 0:
            print("No more cities ")
            raise StopIteration()
        return self._cities.pop()


# Return the Cities order by the length of the city.
@dataclass
class Cities:
    """Cities order by the length of the city."""
    _cities: list = field(default_factory=list)
    Iterator: Callable = field(default=CityIterator)

    def __iter__(self):
        return self.Iterator(self._cities[:])

    @deprecated(active=False)
    def add(self, _city: str):
        self._cities.append(_city)
        return self

    def __str__(self):
        return f"List of cities: {self._cities}"


with open('cities_country.csv') as csvfile:
    csvreader = csv.reader(csvfile)
    next(csvreader)  # skip headers
    cities_by_country = {row[0]: row[4] for row in csvreader}


def transform_cities(_datos:dict):
    d_dict = defaultdict(Cities)
    for key, value in _datos.items():
        d_dict[value].add(key)
    return d_dict


d = transform_cities(cities_by_country)

for item in d:
    print(f"Foreach city in {item}")
    for city in d[item]:
        print(city)

for item in d:
    print(f"Foreach city in {item}")
    if len(item) < 5:
        d[item].Iterator = ReversedCityIterator
    for city in d[item]:
        print(city)

