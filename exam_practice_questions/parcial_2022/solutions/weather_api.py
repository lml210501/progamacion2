from datetime import datetime
from functools import reduce
import requests


def get_tuple_list(response):
    return zip(response['hourly']['time'], response['hourly']['apparent_temperature'], response['hourly']['temperature_2m'])


url = requests.get("https://api.open-meteo.com/v1/forecast?latitude=40.4167&longitude=-3.7033&hourly=temperature_2m,apparent_temperature&current_weather=true&past_days=4")
text = url.json()
print(text)
print(text['hourly']['time'])
print(len(text['hourly']['apparent_temperature']))
print(len(text['hourly']['temperature_2m']))


def string_to_datetime(tuple_):
    date = datetime.strptime(tuple_[0], "%Y-%m-%dT%H:%M")
    return date.hour == 23


lists = get_tuple_list(text)
print(list(map(lambda tup: tup[1], filter(string_to_datetime, lists))))

lists = get_tuple_list(text)

print(list(map(lambda t: round(t[2]-t[1],2), lists)))

lists = get_tuple_list(text)


def max_temperature(prev, actual):
    return actual if prev < actual else prev


print(reduce(max_temperature, map(lambda tup: tup[2], lists), 0))
