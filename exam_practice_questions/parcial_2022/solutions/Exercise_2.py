from typing import Generator, List


def generador(numbers:List) -> Generator[int, str, None]:
    pares = list(reversed(sorted(list(filter(lambda x: x % 2 == 0, numbers)))))

    for number in numbers:
        if number % 2 == 1:
            _exit = yield number
        else:
            _exit = yield pares.pop()
        if _exit == "stop":
            raise StopIteration("Fin")


gen = generador([1,4,3,2])
print(next(gen))
print(next(gen))
try:
    gen.send("stop")
except StopIteration:
    print("Finish iterating")


gen = generador([1,4,3,2])
print(next(gen))
print(next(gen))
gen.send("stop")


####################################


def sort_array(source_array):
    odd = sorted(list(filter(lambda x: x % 2, source_array)))
    l, c = [], 0
    for i in source_array:
        if i in odd:
            val = odd[c]
            c += 1
        else:
            val = i
        _exit = yield val
        if _exit == 'stop':
            raise StopIteration("Finish")




