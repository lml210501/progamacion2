"""Ejercicio 3"""
# Crear un descriptor para el atributo allowance de una clase Capacidad. Llama al descriptor ReduceInteger.
# Se requiere que la variable ReduceInteger no pueda ser inferior a 0 y siempre que se obtenga esa variable,
# se va a ir restando uno hasta llegar a 0, si se siguiese accediendo a ese atributo de la clase, el resultado
# debe continuar siendo 0. Nunca se podrá modificar el atributo si el valor es 0 y deberá informar al usuario de
# que no se ha podido asignar.

import logging

logging.basicConfig(level=logging.INFO)


class LoggedReduce_integerAccess:

    def __get__(self, instanace, owner=None):
        # noinspection PyProtectedMember
        value = instanace._Reduce_integer
        logging.info('Accediendo a la variable %r', value)
        if Reduce_integer < 0:
            print('La variable es inferior a 0')
        else:
            return value

    def __set__(self, instance, value):
        logging.info('Actualizando la variable a %r', value)
        instance._Reduce_integer = value


class Capacidad:
    Reduce_integer = LoggedReduce_integerAccess()  # Descriptor

    def __init__(self, allowance, Reduce_integer):
        self.allowance = allowance        # Regular instance attribute
        self.Reduce_integer = Reduce_integer          # Calls the descriptor

    def subtract(self):
        if self.Reduce_integer == 0:
            print('No se ha podido modificar porque es 0')
        else:
            self.Reduce_integer -= 1

    def __str__(self):
        return str(vars(self))


# Extra: Crear una rutina para cuando se quiera eliminar el atributo. En el momento de la eliminación se le
# preguntará al usuario si está seguro que quiere borrar ese atributo. Si se introduce “y” entonces se procederá
# a borrar la variable, en caso contrario no será borrada y se informará al usuario “No se ha borrado la variable
# {nombre de la variable}”

c = Capacidad(5, 5)
print(c.Reduce_integer)
print(c.Reduce_integer)
print(c.Reduce_integer)

try:
    while True:
        print("¿Esta seguro que quiere eliminar el atributo?")
        Question = input("Porfavor inserta yes (y) or no (n), sólo la primera letra: ")
        if Question == "y":
            print("Procedemos a borrar la variable")
            Reduce_integer = 5
            Capacidad.subtract(Reduce_integer)
        elif Question == "n":
            print("No se ha borrado la variable {nombre de la variable}")
            break
except StopIteration as e:
    print("Stop iteration")