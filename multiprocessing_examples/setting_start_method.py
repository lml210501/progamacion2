import multiprocessing as mp
import time


def foo(q):
    q.put(3)

def sum_numbers(q):
    num_1 = q.get()
    num_2 = q.get()
    print(num_1 + num_2)

if __name__ == '__main__':
    ctx = mp.get_context('spawn')
    q = ctx.Queue()
    num_1 = ctx.Process(target=foo, args=(q,))
    num_2 = ctx.Process(target=foo, args=(q,))
    p2 = ctx.Process(target=sum_numbers, args=(q,))
    num_1.start()
    num_2.start()
    p2.start()
    p2.join()
    print("waiting for queue")