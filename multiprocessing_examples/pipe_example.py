from multiprocessing import Process, Pipe
from time import sleep


def sender(conn, integ):
    print('Worker - started now sleeping for 1 second')
    sleep(1)
    print('Worker - sending data via Pipe')
    conn.send(3 ** 2)
    print('Worker - closing worker end of connection')
    conn.close()


def main():
    print('Main - starting, creating the Pipe')
    main_connection, worker_connection = Pipe()
    print('Main- setting up de processs')
    number = input("Enter a number: ")
    p = Process(target=sender, args=(worker_connection, number))
    print('Main - starting the process')
    p.start()
    print('Main - wait for a response from the child process')
    number_expected = main_connection.recv()
    print(f"We have got: {number_expected}")
    print('Main - closing parent process end of connection')
    main_connection.close()
    print('Main - Done')


if __name__ == '__main__':
    main()
