import multiprocessing as mp
import time

start = time.perf_counter()


def do_something(seconds):
    print(f'Sleeping {seconds} second(s)...')
    time.sleep(seconds)
    print(f'Done Sleeping...{seconds}')


if __name__ == '__main__':
    mp.set_start_method('fork')
    p1 = mp.Process(target=do_something, args=(10,), name='p1')
    p2 = mp.Process(target=do_something, args=(10,), name='p2')

    p1.start()
    p2.start()

    p1.join()
    p2.join()

    finish = time.perf_counter()

    print(f'Finished in {round(finish-start, 2)} second(s)')