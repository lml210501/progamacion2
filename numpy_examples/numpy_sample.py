# Creación de arrays
import numpy as np
array = np.array([1,2,3,4,5])
print(array)

# Acceso a elementos exactamente igual que una lista.
print(array[0]) # 1
print(array[-1]) # 5
print(array[-2]) # 4

# Tipos de datos
# OJO no son listas. `numpy` aporta nuevos tipos de datos. Lo veremos más adelante.
print(type(array)) # <class 'numpy.ndarray'>
print(type(array[0])) # <class 'numpy.int32'>
print(array.dtype) # int32

# Arrays multidimensionales
array_multi = np.array([[1,2,3],
                        [4,5,6],
                        [7,8,9]])
print(array_multi)

# Otra ventaja de usar `numpy` en vez de listas es que el `print` lo realiza en formato matriz,
# lo que nos permite ver los datos mucho mejor que si lo imprimiese en una sola línea.

# Lo mismo que existe el range para crear listas, con inicio, final y salto entre elementos para crear
# listas de numpy objects existe el arange!

# Array del 0 al 9
print(np.arange(10))

# Del 0 al 9, con saltos de 2
print(np.arange(0, 10, 2))

# Array de 0 al 10, con saltos de 0.5
print(np.arange(0, 10, 0.5))

# Especficiando el tipo de dato
print(np.arange(0, 10, 2, np.float64))

# tambien tiene por defecto un random que nos permite manejar cómodamente listas de numpy.
# https://numpy.org/doc/stable/reference/random/index.html?highlight=random#module-numpy.random
print(np.random.rand(10))

# Matriz de numeros aleatorios
print(np.random.randint(5, size = (3,4)))

# Unidimensional
print(np.random.randint(5, 10, size = (10)))

np.random.seed(1234)
print(np.random.rand(10))

np.random.seed(5678)
print(np.random.rand(10))

np.random.seed(None)
print(np.random.rand(10))

# Maneras de crear matrices o vectores o listas
# Matriz identidad
print(np.identity(3))
# Matriz de 0s
np.zeros((3,3))
# Matriz de unos
np.ones((3,3), np.int32)
# Matriz con 100es
np.full((3,3), 100)

matriz = np.random.randint(0,10, size=(3,3))
matriz[0][0]
matriz[:2 , :2]

# 4. Atributos del array
"""
Hay ciertos atributos que debemos conocer:
ndim: es el numero de dimensiones. Número de niveles que tiene el array de numpy.
shape: tamaño de cada una de las dimensiones. Devuelve el resultado en formato tupla
size: cantidad de elementos del array.
itemsize: tamaño en bytes de los items del array
nbytes: tamaño en bytes de todo el array
"""
array_multi = np.array([[1,2,3],
                        [4,5,6],
                        [7,8,9]])
print(array_multi.ndim)
print(array_multi.shape)
print(array_multi.size)

array_multi_3 = np.array([[[1,10], [2, 20], [3, 30]],
                          [[4, 40], [5, 50], [6, 60]],
                          [[7, 70], [8, 80], [9, 90]]])
print(array_multi_3.ndim)
print(array_multi_3.shape)
print(array_multi_3.size)

array = np.array([1,2,3,4,5])
print(array.itemsize)
print(array.nbytes)

array.dtype  # dtype('int32')
# 32 bits para cada elemento
# 32/8 = 4 bytes


array_1 = array
array_2 = np.array([[1,2,3], [4,5,6], [7,8,9]])
array_3 = np.array([[[1,10], [2, 20], [3, 30]],
                    [[4, 40], [5, 50], [6, 60]],
                    [[7, 70], [8, 80], [9, 90]]])

print(array_1.ndim)
print(array_2.ndim)
print(array_3.ndim)

# Indexando arrays numpy
print(array_1[0])
print(array_1[2:5])

array_2 = np.array([[1,2,3], [4,5,6], [7,8,9]])
print(array_2)
print(array_2[0])

# Segundo elemento de la primera fila
print(array_2[0][1])
print(array_2[0,1])

# Tipo del primer elemento
print(type(array_2[0][0]))

# Con el de 3 es igual!!!!
print(array_3[0])
print(array_3[0][-1][-1])
print(array_3[0, -1, -1])

# Slicing! Igual que en listas normales si accedemos a traves de comas, cada coma indica la siguiente dimensión.
# Array de 10 elementos

x = np.arange(10)
print(x)

print("Del primer elemento al quinto", x[:5])

print("Del primero al quinto con saltos de dos", x[:5:2])

print("Desde el quinto", x[4:])
print("Desde el quinto", x[4::])

print("Desde el quinto hasta el penultimo", x[4:-1])

print("Desde el quinto, al primero", x[5::-1])

print("Sacar una copia", x[:])
print("Sacar una copia", x.copy())

print("Ultimos dos elementos", x[-2:])

# Multidimensionalmente funciona igual! Pruebalo! Ejecuta el codigo

array_multi = np.array([[1,2,3,4,5],
                        [6,7,8,9,10],
                        [11,12,13,14,15]])
print(array_multi)

print("2 primeras filas y 3 primeras columnas\n", array_multi[:2:, :3:])

print("Todas las filas, cada dos columnas\n", array_multi[::, ::2])

print("Invertir las filas\n", array_multi[::-1, ::])

print("Invertir columnas\n", array_multi[::, ::-1])


# Máscaras para filtrar el array.
print("filtrar array")
x = np.array([2,4,5,2,3,7,2])
print(x)
print(x == 2)

x_bools = np.array(x == 2)
print(x)
print(x_bools)
print(x[x_bools])
x_bools_mayor_2 = np.array (x > 2)
print(x[x_bools_mayor_2])

#Reshape
print("Reshape!")

x = np.arange(9)
print(x)

y = x.reshape((3,3))
print(y)

y.flatten()

x = np.arange(30).reshape((6,5))
print(x)

x = np.arange(30).reshape((2,3,5))
print(x)

#x = np.array([1,2,3])
# x.reshape(3,4) Esto no funcionaria! Da un error porque no encuentra como rellenar los valores.

# Concatenar arrays
x = np.array([1,2,3])
y = np.array([3,2,1])

print(np.concatenate([x,y]))
z = np.array([99,99,99])
print(np.concatenate([x,y,z]))

#Con array de 2 dimensiones funciona igual
xy = np.array([[1, 2, 3],
              [4, 5 ,6]])
xz = np.array([[1, 2, 3],
              [4, 5 ,6]])

print(np.concatenate([xy, xz], axis =1))

# Para arrays de diferentes dimensiones se puede usar vstack o hstack (vertical u horizontalmente)
xy = np.array([[1,2,3],
              [4,5,6]])

x = np.array([7,8,9])
print(np.vstack([xy, x]))

z = np.array([[7], [8]])
print(np.hstack([xy, z]))

# Sustitución de elementos en numpy.
print( "Sustitución!")

xy = np.array([[1,2,3],
              [4,5,6]])

# Número de dimensiones!
my_filter = ([False, True])
print(xy[my_filter])
# De esta manera, lo que hacemos es preparar una lista de booleanos, y se lo aplicamos al array.
# Si lo queremos hacer de una manera más automática y entendible, utilizamos `where`.
xy = np.array([[1,2,3],
              [4,5,6]])


print(np.where(xy < 4, 10, 20)) # Parecido a un if/else. Si xy < 4, lo sustituyes por 10, sino por 20
print(np.where(xy < 4, 10, xy))

# Copias! Super importante!
# Una caracteristica importante de trabajar con numpy y con arrays es que si creamos subarrays del array
# principal, los elementos siguen existiendo y su valor se actualiza (en el del array principal)
x2 = np.ones((3,4))
print(x2)
# Extraemos una matriz 2x2
x2_sub = x2[:2, :2]
print(x2_sub)
# Si ahora modificamos un elemento del nuevo subarray, verás que el array original ha cambiado
x2_sub[0, 0] = 99
print(x2_sub)
print(x2)

# Esta característica es bastante útil, ya que si trabajamos con datasets muy grandes,
# podremos acceder y procesar partes del dataset original, sin necesidad de hacer copias, y, por tanto,
# sin sobrecargar la memoria.
#
# Aunque siempre tenemos la opción de realizar copias del array mediante la sentencia `copy()`

# Ahora si modificamos el subarray, el original se quedará como estaba
x2_sub_copy = x2[:2, :2].copy()
print(x2_sub_copy)
print(x2)

# Funciones de agregación

"""
|Function Name      |   NaN-safe Version  | Description                                   |
|-------------------|---------------------|-----------------------------------------------|
| ``np.sum``        | ``np.nansum``       | Compute sum of elements                       |
| ``np.prod``       | ``np.nanprod``      | Compute product of elements                   |
| ``np.mean``       | ``np.nanmean``      | Compute mean of elements                      |
| ``np.std``        | ``np.nanstd``       | Compute standard deviation                    |
| ``np.var``        | ``np.nanvar``       | Compute variance                              |
| ``np.min``        | ``np.nanmin``       | Find minimum value                            |
| ``np.max``        | ``np.nanmax``       | Find maximum value                            |
| ``np.argmin``     | ``np.nanargmin``    | Find index of minimum value                   |
| ``np.argmax``     | ``np.nanargmax``    | Find index of maximum value                   |
| ``np.median``     | ``np.nanmedian``    | Compute median of elements                    |
| ``np.percentile`` | ``np.nanpercentile``| Compute rank-based statistics of elements     |
| ``np.any``        | N/A                 | Evaluate whether any elements are true        |
| ``np.all``        | N/A                 | Evaluate whether all elements are true        |

"""
# Agregation functions
print("Agregation")
np.random.seed(1)
s = np.random.randint(50, size=(5,5))
print(s.sum())
print(np.max(s))
print(np.min(s))
print(np.all(s))