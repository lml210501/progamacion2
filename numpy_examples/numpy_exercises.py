# Ejercicio creación numpy
"""
- Crea un array con 3 deportes baloncesto, padel y futbol
- Accede al primer elemento y al ultimo
- Que tipo de dato son?
- Crea una secuencia de numeros de 0 a 10 con saltos de 0.5. EL 0 y el 10 tienen que estar incluidos
- Crea una matriz de 5x2 con numeros enteros aleatorios entre el 10 y el 20
"""


# Ejercicio Slicing

# Crea un array 1D de 0 a 9
# Dame todos los elementos de dos en dos
# Dame los elementos de dos en dos, a partir del 2 elemento
# Dame todos los elementos pero invertidos
# Dame los dos ultimos elementos invertidos
# Dame el completo menos los dos ultimos elementos, invertidos


# Ejercicio

# ¿Cuál es la estatura media de los presidentes de EEUU?
# Las funciones de agregación de Numpy pueden resultar de gran utilidad a la hora de resumir sets de datos.
# En este ejemplo vamos a considerar las diferentes estaturas de los presidentes de Estados Unidos.
# Tienes estos datos disponibles en el CSV -> president_height.csv.
# Calcula: La media, la altura maxima, minima, la desviación típica, y los percentiles 25, 50 y 75.
# Dame el nombre de los presidentes mas alto y más bajo.
