"""In the a.x attribute lookup, the dot operator finds the value 5 stored in the class dictionary.
In the a.y descriptor lookup, the dot operator calls the descriptor’s __get__() method. That
method returns 10. Note that the value 10 is not stored in either the class dictionary or the
instance dictionary. Instead, the value 10 is computed on demand."""

# https://docs.python.org/3/howto/descriptor.html


class Ten:
    def __get__(self, obj, objtype=None):
        return 10


class A:
    x = 5       # Regular class attribute
    y = Ten()   # Descriptor

