import re
from datetime import datetime
from functools import wraps


# Format check decorators
def mandatory_field(func):
    """Should be a value!=None. To be used in getters"""

    @wraps(func)
    def wrapper(self):
        value = func(self)
        if value is None:
            raise MissingFieldError(f'The field {func.__name__} is mandatory and cannot be blank')

        return value

    return wrapper


def timeslot_check(func):
    """Should receive a tuple of datetime objects (start, end). To be used in setters"""

    @wraps(func)
    def wrapper(self, timeslots):
        if not timeslots:
            return None
        elif not isinstance(timeslots, (list, tuple)) or len(timeslots) != 2:
            raise MalformedPayloadError(
                f'Timeslot object {func.__name__} should contain start and end.')
        else:
            for timeslot in timeslots:
                if not isinstance(timeslot, datetime):
                    raise MalformedPayloadError(
                        f'Timeslots should be of type datetime.datetime.')

        return func(self, timeslots)

    return wrapper


def list_strings_type_check(func):
    """Should be a list type of strings. To be used in setters"""

    @wraps(func)
    def wrapper(self, value):
        if value is None:
            return None
        else:
            if not isinstance(value, list):
                raise MalformedPayloadError(f'The field {func.__name__} expects a list[str] input.')

            for item in value:
                if not isinstance(item, str):
                    raise MalformedPayloadError(f'The field {func.__name__} expects a list[str] input.')

        return func(self, value)

    return wrapper


def float_type_check(func):
    """Should be a float-transformable type. To be used in setters"""

    @wraps(func)
    def wrapper(self, value):
        if value is None:
            return None
        else:
            try:
                if isinstance(value, str):
                    pattern = r"^(?:[0-9][,.]?)+(?:[,.][0-9]{2})?$"
                    valid_separators = [',', '.']
                    if re.match(pattern, value):
                        if len(value) > 2:
                            decimal_separator = value[-2] if value[-2] in valid_separators else value[-3]
                            if decimal_separator == ',':
                                value = value.replace('.', '')
                                value = value.replace(',', '.')
                            elif decimal_separator == '.':
                                value = value.replace(',', '')
                            else:
                                value = value.replace(',', '')
                                value = value.replace('.', '')
                        else:
                            value = value.replace(',', '')
                            value = value.replace('.', '')
                    else:
                        raise MalformedPayloadError(
                            f"The value '{value}' for {func.__name__} does not match the decimal number pattern")
                value = float(value)
            except ValueError:
                raise MalformedPayloadError(f'The field {func.__name__} expects a numeric input.')

        return func(self, value)

    return wrapper


def bool_type_check(func):
    """Should be boolean type"""

    @wraps(func)
    def wrapper(self, value):
        if not isinstance(value, bool):
            raise MalformedPayloadError(f'The field {func.__name__} expects a boolean input')

        return func(self, value)

    return wrapper
