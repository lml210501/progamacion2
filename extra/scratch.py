from typing import Callable
from math import log, sin, cos

def f(x: int) -> Callable:
    
    def g(y: int):
        return y ** x

    return g


h = f(3)
print(h(2))


def r(s: Callable, x: int) -> float:
    return s(log(x))


print(r(sin, 1))
print(r(cos, 1))


def log_transform(s: Callable) -> Callable:
    
    def u(x: int) -> float:
        return s(log(x))
    
    return u


seno = log_transform(sin)
seno(10)

@log_transform
def sin_cos(x):
    return sin(x) * cos(x)


sin_cos(10)


def seno_1(x):
    return sin(x)

seno = log_transform(seno_1)

seno(10)


cos_log = log_transform(cos)


def f(x: int) -> int:
    u = 3 * x
    return u


y = f(3)
