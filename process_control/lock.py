# example of a mutual exclusion (mutex) lock for processes
from time import sleep
from random import random
from multiprocessing import Process, Lock


# work function
def task(lock, identifier, value):
    # acquire the lock
    with lock:
        print(f'>process {identifier} got the lock, sleeping for {value}')
        sleep(value)


# entry point
if __name__ == '__main__':
    # create the shared lock
    lock = Lock()
    processes = [Process(target=task, args=(lock, i, random())) for i in range(10)]
    # start the processes
    for process in processes:
        process.start()
    # wait for all processes to finish
    for process in processes:
        process.join()
    # with ProcessPoolExecutor(4) as pool:
    #     item = [(i, lock) for i in range(10)]
    #     processed_list = pool.map(task, range(20))


