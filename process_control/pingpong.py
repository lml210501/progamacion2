# example of using a duplex pipe between processes
from threading import Thread
from time import sleep
from random import random
from multiprocessing import Pipe


# Dos jugadores y ver quien gana primero. Se lanzaran mutuamente numeros aleatorios de 0 a 1 y cada uno tendra un threashold para ganar
# El primero que llege a 10 puntos gana.
def player_1(connection, win_rate):
    # Player 1 always start sending numbers.
    cont = 0
    while cont < 10:
        connection.send(random())
        valor = connection.recv()
        if valor > win_rate:
            cont += 1
    connection.send(-1)


def player_2(connection, win_rate):
    # Player 1 always start sending numbers.
    cont = 0
    win = False
    while cont < 10 or not win:
        valor = connection.recv()
        if valor > win_rate:
            cont += 1
        elif valor == -1:
            win = True
        connection.send(random())
    print("Jugador 2 ha ganado!.")

# generate and send a value
def generate_send(connection, value):
    # generate value
    new_value = random()  # 0 - 1
    # block
    sleep(new_value)
    # update value
    value = value + new_value
    # report
    print(f'>sending {value}', flush=True)
    # send value
    connection.send(value)


# ping pong between processes
def pingpong(connection, win_rate, send_first):
    print('Process Running', flush=True)
    cont = 0
    if send_first:
        generate_send(connection, random())
    while True:
        # read a value
        value = connection.recv()
        # report
        if value > win_rate:
            cont += 1
        print(f'>received {value}', flush=True)
        # send the value back
        generate_send(connection, value)
        # check for stop
        if cont > 10:
            break
    print('Process Done', flush=True)


def send_order(connection, order):
    connection.send(order)

def receive_order(connection):
    total_ganado = 0
    while True:
        pedido = connection.recv()
        total_ganado += pedido["amount"]
        print(f"Total ganado: {total_ganado}")
        if total_ganado > 1500:
            break

    print("Done")

# entry point
if __name__ == '__main__':
    # create the pipe
    conn1, conn2 = Pipe(duplex = True)
    # create players
    player1 = Thread(target=player_1, args=(conn1, 0.8))
    player2 = Thread(target=player_2, args=(conn2, 0.6))
    # start players
    player1.start()
    player2.start()
    # wait for players to finish
    player1.join()
    player2.join()
