from multiprocessing import Lock
from threading import Thread, Event
from time import sleep, time


def modify_variable(var, lock: Lock):
    t0 = time()
    while time() - t0 < 5:
        lock.acquire()
        try:
            for i in range(len(var)):
                var[i] += 1
        finally:
            lock.release()
        print('Stop printing')



my_var = [1, 2, 3]
lock = Lock()
t = Thread(target=modify_variable, args=(my_var, lock))
t2 = Thread(target=modify_variable, args=(my_var, lock))
t.start()
t2.start()
t0 = time()
while time() - t0 < 5:
    print(my_var)
t.join()
t2.join()
print(my_var)