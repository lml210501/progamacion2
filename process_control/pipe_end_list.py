import multiprocessing
from time import sleep


def producer(conn, events):
    for event in events:
        conn.send(event)
        print(f"Event Sent: {event}")
        sleep(.1)


def consumer(conn):
    while True:
        event = conn.recv()
        if event == "eod":
            print("Event Received: End of Day")
            return
        print(f"Event Received: {event}")


def main():
    events = ["get up", "brush your teeth", "shower", "work", "eod"]
    conn1, conn2 = multiprocessing.Pipe()
    process_1 = multiprocessing.Process(target=producer, args=(conn1, events))
    process_2 = multiprocessing.Process(target=consumer, args=(conn2,))
    process_1.start()
    process_2.start()
    process_1.join()
    process_2.join()


if __name__ == "__main__":
    main()