# example of using a duplex pipe between processes
from threading import Thread
from time import sleep
from random import random
from multiprocessing import Pipe


# Dos jugadores y ver quien gana primero. Se lanzaran mutuamente numeros aleatorios de 0 a 1 y cada uno tendra un threashold para ganar
# El primero que llege a 10 puntos gana.

def send_order(connection, order):
    connection.send(order)

def receive_order(connection):
    total_ganado = 0
    while True:
        pedido = connection.recv()
        total_ganado += pedido["amount"]
        print(f"Total ganado: {total_ganado}")
        if total_ganado > 70:
            break

    print("Done")

# entry point
if __name__ == '__main__':
    # create the pipe
    pedido1 = {
        "id": 1,
        "To": "Madrid",
        "from": "Galicia",
        "currency": "EUR",
        "amount": 35
    }
    pedido2 = {
        "id": 1,
        "To": "Madrid",
        "from": "Galicia",
        "currency": "EUR",
        "amount": 37
    }
    conn1, conn2 = Pipe(duplex = False)
    orders = [pedido1, pedido2]
    # create players
    for order in orders:
        t = Thread(target=send_order, args=(conn2, order))
        t.start()
    p = Thread(target=send_order, args=(conn2, pedido2))
    player2 = Thread(target=receive_order, args=(conn1,))
    player2.start()
